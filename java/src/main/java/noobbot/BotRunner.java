package noobbot;

/**
 * Created by paul on 4/21/14.
 */
public interface BotRunner {

    DriveCommand getDriveCommand(Game game);
    void crashed();
}
