package noobbot;

import noobbot.msg.Car;
import noobbot.msg.CarId;
import noobbot.msg.CarPosition;

/**
 * Created by paul on 4/17/14.
 */
public class CarState {

    public final Car.CarDimensions deminsions;
    public final CarId carId;
    public CarPosition position;
    public double throttle;
    public double distanceTraveled;
    public double speed;
    public double acceleration;
    public int gameTick;
    public boolean isCrashed;

    public CarState(Car car) {
        deminsions = car.dimensions;
        carId = car.id;
    }


    /**
     * Must be called on *EVERY* game tick to keep accurate data about the state of
     * this car
     * @param game
     * @param carPosition
     */
    public void updatePosition(Game game, CarPosition carPosition) {

        if(!carId.equals(carPosition.id)){
            throw new RuntimeException("CarIDs should match!");
        }

        gameTick++;

        double newDistanceTraveled;

        if(position == null){
            newDistanceTraveled = carPosition.piecePosition.inPieceDistance;
        } else {
            newDistanceTraveled = game.mRace.track.getDistanceTraveled(position.piecePosition, carPosition.piecePosition);
        }
        this.distanceTraveled += newDistanceTraveled;

        double newSpeed = (newDistanceTraveled);
        acceleration = (newSpeed-speed);
        speed = newSpeed;
        position = carPosition;
        isCrashed = false;
    }

    /**
     * Prints the current readings to stdout
     */
    public void printInfo() {
        System.out.println(String.format(
                "Throttle: %3.3f | Speed: % 3.4f units/tick | Acceleration: % 3.4f units/tick^2 " +
                        "| GameTick: %4s | Position: % 3.4f | Angle: % 1.3f",
                throttle, speed, acceleration, gameTick, distanceTraveled, position.angle));
    }
}
