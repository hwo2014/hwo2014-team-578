package noobbot;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import noobbot.msg.MsgTypeAdapter;
import noobbot.msg.Ping;
import noobbot.msg.RaceMsg;
import noobbot.msg.SendMsg;
import noobbot.utils.ThreadUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.Socket;
import java.util.Set;
import java.util.concurrent.*;

/**
 * Created by paul on 4/21/14.
 */
public class Client {


    public interface RaceMessageListener {
        void onRaceMessage(Client client, RaceMsg msg);
    }

    final static Logger logger = LoggerFactory.getLogger(Client.class);

    private Socket socket;
    private PrintWriter writer;
    private BufferedReader reader;
    public static final ExecutorService mainThread = Executors.newSingleThreadExecutor();
    private Future<?> mInputProcessing;
    private boolean mIsRunning = false;
    public final Set<RaceMessageListener> mListeners = new CopyOnWriteArraySet<RaceMessageListener>();

    public void connectToServer(String host, int port) throws IOException {
        System.out.println("Connecting to " + host + ":" + port);

        socket = new Socket(host, port);
        writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));
        reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        startInputProcessing();
    }

    private void startInputProcessing() {
        mIsRunning = true;
        mInputProcessing = ThreadUtils.IOThreads.submit(new Runnable(){

            @Override
            public void run() {
                try {
                    String line = null;
                    while(mIsRunning && (line = reader.readLine()) != null) {
                        try {
                            Gson gson = new GsonBuilder()
                                    .registerTypeAdapter(RaceMsg.class, new MsgTypeAdapter())
                                    .create();
                            final RaceMsg msg = gson.fromJson(line, RaceMsg.class);
                            if(msg == null) {
                                throw new JsonSyntaxException(line);
                            } else {
                                processMessage(msg);
                            }
                        } catch(JsonSyntaxException e) {
                            logger.warn(String.format("unknown message from server: %s", line));
                            send(new Ping());
                        }
                    }

                } catch (Throwable e){
                    logger.error("Bot read error. exiting.", e);
                } finally {
                    logger.info("Shutting down client");
                    synchronized(Client.this){
                        mIsRunning = false;
                        Client.this.notifyAll();
                    }
                }
            }
        });
    }

    public void shutdown() {
        mIsRunning = false;
        try {
            mInputProcessing.get();
        } catch (Exception e) {
            logger.error("", e);
        }
    }

    public boolean isRunning() {
        return mIsRunning;
    }

    private void processMessage(final RaceMsg msg) {
        mainThread.execute(new Runnable(){

            @Override
            public void run() {
            for(final RaceMessageListener l : mListeners){
                l.onRaceMessage(Client.this, msg);
            }
            }
        });
    }

    public void send(final SendMsg msg) {
        logger.debug("sending message: " + msg);
        writer.println(msg.toJson());
        writer.flush();
    }
}
