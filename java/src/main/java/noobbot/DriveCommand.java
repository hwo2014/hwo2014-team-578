package noobbot;

/**
 * Created by paul on 4/21/14.
 */
public class DriveCommand {

    public enum Type {
        THROTTLE,
        SWITCHLINE,
        TURBO
    }

    public Type type;
    public double throttle;
    public int lane;

    private DriveCommand(Type type){
        this.type = type;
    }

    public static DriveCommand setThrottle(double throttle){
        DriveCommand cmd = new DriveCommand(Type.THROTTLE);
        cmd.throttle = throttle;
        return cmd;
    }

    public static DriveCommand switchLane(int lane) {
        DriveCommand cmd = new DriveCommand(Type.SWITCHLINE);
        cmd.lane = lane;
        return cmd;
    }

}
