package noobbot;

import noobbot.msg.*;
import noobbot.racestate.GameInput;
import noobbot.racestate.GameState;
import noobbot.racestate.StateMachine;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by paul on 4/21/14.
 */
public class Game extends StateMachine<GameState, GameInput> implements Client.RaceMessageListener {

    public Client client = new Client();
    public Race mRace;
    private CarId mOurCar;
    public BotRunner mBotRunner;
    public ArrayList<CarState> cars = new ArrayList<CarState>();

    public Game() {
        super(GameState.PREJOIN);
        configure(GameState.PREJOIN, GameInput.GAMEINIT, GameState.QUALIFING);
        configure(GameState.QUALIFING, GameInput.GAMESTART, GameState.QUALIFING);
        configure(GameState.QUALIFING, GameInput.CARPOSITIONS, GameState.QUALIFING);
        configure(GameState.QUALIFING, GameInput.GAMEEND, GameState.PRERACE);
        configure(GameState.PRERACE, GameInput.GAMEINIT, GameState.RACEING);
        configure(GameState.RACEING, GameInput.GAMEEND, GameState.ENDED);

        client.mListeners.add(this);
    }

    public void join(String host, int port, String botName, String botKey) throws IOException {
        client.connectToServer(host, port);
        client.send(new Join(botName, botKey));
    }

    @Override
    public void onRaceMessage(Client client, RaceMsg msg) {

        if(msg instanceof GameInitMsg){
            GameInitMsg gameInit = (GameInitMsg)msg;
            mRace = gameInit.race;
            cars.clear();
            for(Car car : gameInit.race.cars){
                cars.add(new CarState(car));
            }
            input(GameInput.GAMEINIT, null);

        } else if(msg instanceof YourCarMsg){
            YourCarMsg yourCarMsg = (YourCarMsg) msg;
            mOurCar = new CarId();
            mOurCar.name = yourCarMsg.name;
            mOurCar.color = yourCarMsg.color;

        } else if(msg instanceof GameStartMsg){
            GameStartMsg gameStart = (GameStartMsg)msg;
            input(GameInput.GAMESTART, gameStart);

        } else if(msg instanceof CrashMsg){
            final CrashMsg crashMsg = (CrashMsg)msg;
            CarId crashedCar = new CarId();
            crashedCar.name = crashMsg.name;
            crashedCar.color = crashMsg.color;

            CarState carState = getCarState(crashedCar);
            carState.isCrashed = true;

            if(crashedCar.equals(mOurCar)){
                mBotRunner.crashed();
            }

            //reset acceleration
            carState.acceleration = 0.0;

        } else if(msg instanceof CarPositionsMsg){
            CarPositionsMsg carPositionsMsg = (CarPositionsMsg)msg;
            input(GameInput.CARPOSITIONS, carPositionsMsg);

            for(CarPosition carPosition : carPositionsMsg.data){
                CarState carState = getCarState(carPosition.id);
                if(carState != null){
                    carState.updatePosition(this, carPosition);
                }
            }

            if(mBotRunner != null){
                DriveCommand cmd = mBotRunner.getDriveCommand(this);
                if(cmd == null){
                    client.send(new Ping());
                } else {
                    switch(cmd.type){
                        case THROTTLE:
                            client.send(new Throttle(cmd.throttle));
                            getCarState(mOurCar).throttle = cmd.throttle;
                            break;
                        case SWITCHLINE:
                            //TODO: implement this
                            break;
                        case TURBO:
                            //TODO: implement this
                            break;
                    }
                }
            }

        } else {
            client.send(new Ping());
        }
    }

    public CarState getCarState(CarId carid) {
        CarState retval = null;
        for(CarState state : cars){
            if(state.carId.equals(carid)){
                retval = state;
                break;
            }
        }
        return retval;
    }

    public void waitForGameToEnd() {
        while(getState() != GameState.ENDED && client.isRunning()){
            synchronized (this){
                try {
                    wait(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public CarId getOurCarId() {
        return mOurCar;
    }
}
