package noobbot;

import noobbot.botrunner.MikeThrottlerRunner;

import java.io.IOException;

public class Main {

    public static Game game = new Game();

    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        game.mBotRunner = new MikeThrottlerRunner();

        game.join(host, port, botName, botKey);

        game.waitForGameToEnd();
    }


}
