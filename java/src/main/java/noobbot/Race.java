package noobbot;

import noobbot.msg.Car;
import noobbot.msg.RaceSession;
import noobbot.msg.Track;

/**
 * Created by mikemole on 4/19/14.
 */
public class Race {
    public Track track;
    public Car[] cars;
    public RaceSession raceSession;

}
