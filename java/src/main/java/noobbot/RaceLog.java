package noobbot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.OutputStream;

import noobbot.msg.CarId;
import noobbot.msg.RaceMsg;
import noobbot.msg.YourCarMsg;
import noobbot.utils.AppendList;

/**
 * Created by paul on 4/17/14.
 */
public class RaceLog implements Client.RaceMessageListener{

    final static Logger logger = LoggerFactory.getLogger(RaceLog.class);

    AppendList<RaceMsg> log = new AppendList<RaceMsg>();

    @Override
    public void onRaceMessage(Client client, RaceMsg msg) {
        log.add(msg);
    }


    public CarId getOurCarId() {
        CarId retval = null;

        for(RaceMsg msg : log){
            if(msg instanceof YourCarMsg){
                retval = new CarId();
                retval.color = ((YourCarMsg) msg).color;
                retval.name = ((YourCarMsg) msg).name;
                break;
            }
        }
        return retval;
    }

    public void writeToOutputStream(OutputStream out) {

    }



}
