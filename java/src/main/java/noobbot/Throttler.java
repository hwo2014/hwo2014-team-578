package noobbot;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by mikemole on 4/16/14.
 */
public class Throttler {

    private double lastValue;
    private double lastAngle;

    private static final double STEP_SIZE = 0.20;

    public Throttler(double startValue) {
        lastValue = startValue;
        lastAngle = 0;
    }

    public double nextValue(final double currAngle) {
        double nextValue = lastValue;

        BigDecimal roundedCurrAngle = new BigDecimal(currAngle);
        roundedCurrAngle.setScale(3, RoundingMode.HALF_UP);
        BigDecimal roundedLastAngle = new BigDecimal(lastAngle);
        roundedLastAngle.setScale(3, RoundingMode.HALF_UP);

        if (Math.abs(roundedCurrAngle.doubleValue()) > Math.abs(roundedLastAngle.doubleValue())) {
            nextValue -= STEP_SIZE;
//            System.out.println(String.format("Stepping down. curr: %s, last: %s", roundedCurrAngle, roundedLastAngle));
        } else {
            nextValue += STEP_SIZE;
        }

        if (nextValue > 1) {
            nextValue = 1;
        } else if (nextValue < STEP_SIZE) {
            nextValue = STEP_SIZE;
        }



        lastValue = nextValue;
        lastAngle = currAngle;

        return nextValue;
    }

    public double getLastValue() {
        return lastValue;
    }
}
