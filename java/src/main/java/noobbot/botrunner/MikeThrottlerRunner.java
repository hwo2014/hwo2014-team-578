package noobbot.botrunner;

import noobbot.*;
import noobbot.msg.CarId;
import noobbot.msg.CarPosition;
import noobbot.msg.Track;

/**
 * Created by paul on 4/27/14.
 */
public class MikeThrottlerRunner implements BotRunner {

    public static final double UNKNOWN = -1.0;

    double mCrashVelocity = UNKNOWN;
    private double mLastSpeed;
    private double mLastThrottle;
    private CarPosition mLastPosition;
    private CarPosition mCrashPosition;
    private Game mGame;

    private Throttler mThrottler;

    public MikeThrottlerRunner() {
        mThrottler = new Throttler(1.0);
    }

    @Override
    public DriveCommand getDriveCommand(Game game) {

        DriveCommand retval = null;

        final CarId ourCar = game.getOurCarId();
        CarState carState = game.getCarState(ourCar);
        carState.printInfo();

        if (mGame == null) {
            retval = DriveCommand.setThrottle(1.0);
        } else {
            double angle = carState.position.angle;
            if (approachingTurn(carState)) {
                retval = DriveCommand.setThrottle(Math.min(0.5, mThrottler.getLastValue()));
            } else {
                retval = DriveCommand.setThrottle(mThrottler.nextValue(angle));
            }
        }

        mLastSpeed = carState.speed;
        mLastThrottle = carState.throttle;
        mLastPosition = carState.position;
        mGame = game;
        return retval;
    }

    private boolean approachingTurn(CarState carState) {
        final int nextTurnPeiceIndex = getUpcomingTurn(carState.position.piecePosition.pieceIndex);
        final Track.TrackPiece peice = mGame.mRace.track.pieces.get(nextTurnPeiceIndex);

        boolean turnIsNext = carState.position.piecePosition.pieceIndex == nextTurnPeiceIndex - 1;
        boolean carIsAlmostThroughPiece = carState.position.piecePosition.inPieceDistance
                > 0.75*mGame.mRace.track.pieces.get(nextTurnPeiceIndex-1).length;

        if (turnIsNext && carIsAlmostThroughPiece) {
            return true;
        } else {
            return false;
        }
    }


    @Override
    public void crashed() {
//        mVelocityModel.run();
        mCrashVelocity = mLastSpeed;
        mCrashPosition = mLastPosition;
    }

    private int getUpcomingTurn(int peiceIndex) {
        int retval = -1;
        while(true){
            peiceIndex = (peiceIndex+1)%mGame.mRace.track.pieces.size();
            Track.TrackPiece peice = mGame.mRace.track.pieces.get(peiceIndex);
            if(Math.abs(mGame.mRace.track.pieces.get(peiceIndex).angle) > 0.0){
                retval = peiceIndex;
                break;
            }
        }
        return retval;
    }


}
