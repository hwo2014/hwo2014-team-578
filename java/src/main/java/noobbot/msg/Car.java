package noobbot.msg;

/**
 * Created by mikemole on 4/19/14.
 */
public class Car {
    public final CarId id;
    public final CarDimensions dimensions;

    public Car(final CarId id, final CarDimensions dimensions) {
        this.id = id;
        this.dimensions = dimensions;
    }

    public static final class CarDimensions {
        public final double length;
        public final double width;
        public final double guideFlagPosition;

        public CarDimensions(final double length, final double width, final double guideFlagPosition) {
            this.length = length;
            this.width = width;
            this.guideFlagPosition = guideFlagPosition;
        }
    }
}
