package noobbot.msg;

/**
 * Created by mikemole on 4/16/14.
 */
public class CarId {

    public String name;
    public String color;

    public CarId() {}

    public CarId(YourCarMsg car) {
        this.name = car.name;
        this.color = car.color;
    }

    @Override
    public boolean equals(Object obj) {
        boolean retval = false;
        if(obj instanceof CarId){
            retval = color.equals(((CarId) obj).color);
        } else if(obj instanceof String){
            retval = color.equals(obj);
        }
        return retval;
    }

    @Override
    public int hashCode() {
        return color.hashCode();
    }
}
