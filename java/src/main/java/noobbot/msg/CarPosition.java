package noobbot.msg;

import com.google.gson.*;

/**
 * Created by mikemole on 4/16/14.
 */
public class CarPosition {
    public final CarId id;
    public final double angle;
    public final PiecePosition piecePosition;

    public CarPosition(final CarId id, final double angle,
                       final PiecePosition piecePosition) {
        this.id = id;
        this.angle = angle;
        this.piecePosition = piecePosition;
    }

    public static final class PiecePosition {
        public final int pieceIndex;
        public final double inPieceDistance;
        public final Lane lane;
        public final int lap;

        public PiecePosition(final int pieceIndex, final double inPieceDistance,
                             final Lane lane, final int lap) {
            this.pieceIndex = pieceIndex;
            this.inPieceDistance = inPieceDistance;
            this.lane = lane;
            this.lap = lap;
        }

        public static final class Lane {
            public final int startLaneIndex;
            public final int endLaneIndex;

            public Lane(final int startLaneIndex, final int endLaneIndex) {
                this.startLaneIndex = startLaneIndex;
                this.endLaneIndex = endLaneIndex;
            }
        }
    }

}
