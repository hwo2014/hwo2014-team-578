package noobbot.msg;

/**
 * Created by mikemole on 4/17/14.
 */
public class CarPositionsMsg extends RaceMsg {

    public CarPosition[] data;
    public String gameId;
    public int gameTick;

}
