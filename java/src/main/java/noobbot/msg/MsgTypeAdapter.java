package noobbot.msg;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

/**
 * Created by paul on 4/18/14.
 */
public class MsgTypeAdapter extends TypeAdapter<RaceMsg> {

    Gson gson;

    public MsgTypeAdapter() {
        gson = new GsonBuilder().create();
    }

    @Override
    public void write(JsonWriter out, RaceMsg value) throws IOException {

    }

    @Override
    public RaceMsg read(JsonReader in) throws IOException {
        RaceMsg retval = null;
        JsonObject obj = gson.fromJson(in, JsonObject.class);

        String msgType = obj.get("msgType").getAsString();

        if("gameStart".equals(msgType)){
            retval = new GameStartMsg();
        } else if("carPositions".equals(msgType)){
            retval = gson.fromJson(obj, CarPositionsMsg.class);
        } else if("yourCar".equals(msgType)) {
            retval = gson.fromJson(obj.get("data"), YourCarMsg.class);
        } else if("gameInit".equals(msgType)){
            retval = gson.fromJson(obj.get("data"), GameInitMsg.class);
        } else if("crash".equals(msgType)){
            retval = gson.fromJson(obj.get("data"), CrashMsg.class);
        }

        return retval;
    }
}
