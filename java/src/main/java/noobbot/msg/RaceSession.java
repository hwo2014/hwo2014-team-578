package noobbot.msg;

/**
 * Created by mikemole on 4/16/14.
 */
public class RaceSession {
    public final int laps;
    public final int maxLapTimeMs;
    public final boolean quickRace;

    public RaceSession(final int laps, final int maxLapTimeMs, final boolean quickRace) {
        this.laps = laps;
        this.maxLapTimeMs = maxLapTimeMs;
        this.quickRace = quickRace;
    }

    @Override
    public String toString() {
        return String.format("Laps: %s, maxLapTimeMs: %s, quickRace: %s",
                laps, maxLapTimeMs, quickRace);
    }
}
