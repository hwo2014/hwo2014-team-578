package noobbot.msg;


import java.util.ArrayList;

public class Track {

    public static class TrackPiece {
        public int index;
        public double length;
        public double angle;
        public double radius;

        public double getLaneLength(double distanceFromCenter) {
            double retval = 0.0;
            if (angle == 0.0) {
                retval += length;
            } else {
                //calc circumference
                double adjRadius = radius;
                if(angle > 0) {
                    adjRadius -= distanceFromCenter;
                } else {
                    adjRadius += distanceFromCenter;
                }
                retval += (2 * Math.PI * adjRadius) * Math.abs(angle) / 360.0;
            }
            return retval;
        }
    }

    public static class Lane {
        public double distanceFromCenter;
        int index;
    }

    public String id;
    public String name;
    public ArrayList<TrackPiece> pieces = new ArrayList<TrackPiece>();
    public ArrayList<Lane> lanes = new ArrayList<Lane>();


    public double getDistanceTraveled(CarPosition.PiecePosition start, CarPosition.PiecePosition end) {
        double retval = 0.0;

        if(start.pieceIndex == end.pieceIndex){
            retval = end.inPieceDistance - start.inPieceDistance;
        } else {

            //TODO: this calculation does not account for changing lanes
            double lineDistanceFromCenter = lanes.get(start.lane.startLaneIndex).distanceFromCenter;
            retval += pieces.get(start.pieceIndex).getLaneLength(lineDistanceFromCenter) - start.inPieceDistance;

            for(int i=start.pieceIndex+1;i<end.pieceIndex;i++){
                lineDistanceFromCenter = lanes.get(start.lane.startLaneIndex).distanceFromCenter;
                retval += pieces.get(start.pieceIndex).getLaneLength(lineDistanceFromCenter);
            }

            retval += end.inPieceDistance;
        }

        return retval;
    }

}
