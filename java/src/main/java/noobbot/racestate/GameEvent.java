package noobbot.racestate;

/**
 * Created by paul on 4/21/14.
 */
public class GameEvent {

    public final GameInput type;
    public Object data;

    public GameEvent(GameInput type) {
        this.type = type;
    }

    @Override
    public int hashCode() {
        return type.hashCode();
    }


}
