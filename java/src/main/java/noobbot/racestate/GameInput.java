package noobbot.racestate;

/**
 * Created by paul on 4/21/14.
 */
public enum GameInput {

    GAMEINIT,
    GAMESTART,
    CARPOSITIONS,
    CRASH,
    SPAWN,
    LAPFINISHED,
    GAMEEND,
    TOURNAMENTEND

}
