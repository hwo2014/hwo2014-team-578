package noobbot.racestate;

/**
 * Created by paul on 4/21/14.
 */
public enum GameState {

    PREJOIN,
    JOINED,
    QUALIFING,
    PRERACE,
    RACEING,
    ENDED

}
