package noobbot.utils;

import java.util.Iterator;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class AppendList<E> implements Iterable<E> {

    private static class Node<E> {
        E data;
        Node<E> next;

        public Node(E data){
            this.data = data;
        }
    }

    private static class ConcurentIterator<E> implements Iterator<E> {

        private Node<E> node;
        private final int size;
        private int pos = 0;

        ConcurentIterator(Node<E> start, int size){
            this.node = start;
            this.size = size;
        }

        @Override
        public boolean hasNext() {
            return pos < size;
        }

        @Override
        public E next() {
            E retval = node.data;
            node = node.next;
            pos++;
            return retval;
        }

        @Override
        public void remove() {
            throw new NotImplementedException();
        }
    }

    private int size;
    private Node head;
    private Node tail;

    public synchronized void add(E element) {
        Node<E> newNode = new Node<E>(element);
        if(head != null) {
            head.next = newNode;
        }

        head = newNode;
        size++;
    }

    public synchronized int size() {
        return size;
    }

    @Override
    public synchronized Iterator<E> iterator() {
        return new ConcurentIterator<E>(tail, size);
    }
}
