package noobbot.utils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by paul on 4/17/14.
 */
public class ThreadUtils {

    public static final ExecutorService IOThreads = Executors.newCachedThreadPool();
}
