package noobbot;

import ch.qos.logback.classic.Level;
import noobbot.botrunner.MikeThrottlerRunner;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Created by mike on 4/28/14.
 */
public class TestRunMikeThrottler {

    public Game game = new Game();

    public void setUp(String host, int port, String botName, String botKey) throws IOException {
        game.mBotRunner = mBotRunner;
        game.join(host, port, botName, botKey);
    }

    @Test
    public void testRunRace() throws Exception {
        ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        root.setLevel(Level.INFO);


        setUp("hakkinen.helloworldopen.com", 8091, "Team BurstMike", "ogG4smbgojQtCw");

        game.waitForGameToEnd();
        System.out.println("done");
    }

    private BotRunner mBotRunner = new MikeThrottlerRunner();
}
